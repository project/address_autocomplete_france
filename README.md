Address autocomplete (France)
===================

INTRODUCTION
------------

This module uses
the [locality API provided by the French government](https://geo.api.gouv.fr/decoupage-administratif/communes)
to add an autocomplete feature on French address fields.
It suggest cities based on the postal code entered by the user.


REQUIREMENTS
------------

 * [Address](https://www.drupal.org/project/address)
 * [php-api-fr-gouv-api-geo-object](https://gitlab.com/php-extended/php-api-fr-gouv-api-geo-object)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   <https://www.drupal.org/documentation/install/modules-themes/modules-8>
   for further information.
   Installing with Composer is recommended
   because it will install php-api-fr-gouv-api-geo-object automatically as well.
   

CONFIGURATION
-------------

To use this module, you need to select the *Address autocomplete*
widget for your address field
in your entity form mode configuration.


MAINTAINERS
-----------

Current maintainers:

 * Pierre Rudloff (prudloff) - <https://www.drupal.org/u/prudloff>

This project has been sponsored by:

 * Insite:
    A lasting and independent business project
    with more than 20 years experience
    and several hundred references in web projects and visual communication
    with public actors, associative and professional networks.
    Visit <https://www.insite.coop/> for more information.
