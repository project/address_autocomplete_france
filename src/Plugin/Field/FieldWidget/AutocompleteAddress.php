<?php

namespace Drupal\address_autocomplete_france\Plugin\Field\FieldWidget;

use Drupal\address\Plugin\Field\FieldWidget\AddressDefaultWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Extends the address widget to use our custom element.
 *
 * @FieldWidget(
 *   id = "address_autocomplete_france",
 *   label = @Translation("Address autocomplete (France)"),
 *   field_types = {
 *     "address"
 *   },
 * )
 */
class AutocompleteAddress extends AddressDefaultWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element['address']['#type'] = 'address_autocomplete_france';

    return $element;
  }

}
