<?php

namespace Drupal\address_autocomplete_france\Controller;

use Drupal\address_autocomplete_france\PostalCodeGeoEndpoint;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Controller\ControllerBase;
use GuzzleHttp\Client;
use Http\Adapter\Guzzle7\Client as AdapterClient;
use Psr\Http\Client\ClientExceptionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Autocomplete controller that searches a locality.
 *
 * @package Drupal\address_autocomplete_france\Controller
 */
class LocalityController extends ControllerBase {

  /**
   * HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  private Client $client;

  /**
   * Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private TimeInterface $time;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): LocalityController {
    $instance = parent::create($container);
    $instance->client = $container->get('http_client');
    $instance->time = $container->get('datetime.time');

    return $instance;
  }

  /**
   * Call the geo.api.gouv.fr API.
   *
   * @param string $query
   *   Search query.
   *
   * @return array
   *   API result.
   *
   * @throws \PhpExtended\DataProvider\UnprovidableThrowable
   * @throws \PhpExtended\Reifier\MissingImplementationThrowable
   * @throws \PhpExtended\Reifier\MissingInnerTypeThrowable
   * @throws \PhpExtended\Reifier\MissingParserThrowable
   * @throws \PhpExtended\Reifier\ReificationThrowable
   * @link https://geo.api.gouv.fr/decoupage-administratif/communes
   */
  private function callApi(string $query): array {
    $json = [];

    $endpoint = new PostalCodeGeoEndpoint(new AdapterClient($this->client));

    try {
      foreach ($endpoint->getCommunesFromPostalCode($query) as $result) {
        $name = $result->getNom();
        $json[] = [
          'label' => $name,
          // This is a bit ugly but the AJAX callback needs both these info.
          'value' => $query . ':' . $name,
        ];
      }
    }
    catch (ClientExceptionInterface $e) {
      $this->getLogger('address_autocomplete_france')
        ->error($this->t('Could not get locality: @error', ['@error' => $e->getMessage()]));
    }

    return $json;
  }

  /**
   * Search localities by their postal code.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Response.
   * @throws \PhpExtended\DataProvider\UnprovidableThrowable
   * @throws \PhpExtended\Reifier\MissingImplementationThrowable
   * @throws \PhpExtended\Reifier\MissingInnerTypeThrowable
   * @throws \PhpExtended\Reifier\MissingParserThrowable
   * @throws \PhpExtended\Reifier\ReificationThrowable
   */
  public function getByPostalCode(Request $request): JsonResponse {
    $cache = $this->cache();

    $json = [];

    if ($request->query->has('q')) {
      $query = $request->query->get('q');

      // No need to call the API if it is obviously not a postal code.
      if (is_numeric($query) && strlen($query) == 5) {
        $cid = implode(':', [
          'address_autocomplete_france',
          'locality',
          $query,
        ]);

        if ($cached = $cache->get($cid)) {
          $json = $cached->data;
        }
        else {
          $json = $this->callApi($query);

          $cache->set($cid, $json, $this->time->getCurrentTime() + 86400);
        }
      }
    }

    return new JsonResponse($json);
  }

}
