<?php

namespace Drupal\address_autocomplete_france\Element;

use Drupal\address\Element\Address;

/**
 * Extends the address element to add autocomplete on postal code/locality.
 *
 * @FormElement("address_autocomplete_france")
 */
class AutocompleteAddress extends Address {

  /**
   * {@inheritdoc}
   */
  protected static function addressElements(array $element, array $value): array {
    $element = parent::addressElements($element, $value);

    if ($value['country_code'] == 'FR') {
      $element['postal_code']['#autocomplete_route_name'] = 'address_autocomplete_france.locality';
      $element['#attached']['library'][] = 'address_autocomplete_france/autocomplete';
    }

    return $element;
  }

}
