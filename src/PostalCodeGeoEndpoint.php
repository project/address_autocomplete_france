<?php

namespace Drupal\address_autocomplete_france;

use Drupal\Core\Url;
use PhpExtended\ApiFrGouvApiGeo\ApiFrGouvApiGeoCommune;
use PhpExtended\ApiFrGouvApiGeo\ApiFrGouvApiGeoEndpoint;
use PhpExtended\DataProvider\JsonStringDataProvider;

/**
 * Extended endpoint to get cities from postal code.
 */
class PostalCodeGeoEndpoint extends ApiFrGouvApiGeoEndpoint {

  /**
   * @param string $postalCode
   *
   * @return \PhpExtended\ApiFrGouvApiGeo\ApiFrGouvApiGeoCommune[]
   * @throws \PhpExtended\DataProvider\UnprovidableThrowable
   * @throws \PhpExtended\Reifier\MissingImplementationThrowable
   * @throws \PhpExtended\Reifier\MissingInnerTypeThrowable
   * @throws \PhpExtended\Reifier\MissingParserThrowable
   * @throws \PhpExtended\Reifier\ReificationThrowable
   * @throws \Psr\Http\Client\ClientExceptionInterface
   */
  public function getCommunesFromPostalCode(string $postalCode): array {
    $uri = $this->_uriFactory->createUri(
      Url::fromUri(
        self::HOST . '/communes', ['query' => ['codePostal' => $postalCode]]
      )->toString()
    );
    $request = $this->_requestFactory->createRequest('GET', $uri);
    $response = $this->_httpClient->sendRequest($request);
    $json = new JsonStringDataProvider($response->getBody()->__toString());

    return $this->_reifier->reifyAll(ApiFrGouvApiGeoCommune::class, $json->provideAll());
  }

}
