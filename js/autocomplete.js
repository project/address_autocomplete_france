/* global Drupal, jQuery */

Drupal.behaviors.addressAutocompleteFrance = {
  /**
   * @param e
   */
  fillLocality: function (e) {
    const $target = jQuery(e.target);
    const value = $target.val().split(':');

    $target.val(value[0]);

    if (value[1]) {
      $target.closest(
        '.address-container-inline'
      ).find('input.locality').val(value[1]);
    }
  },
  /**
   * @param context
   */
  attach: function (context) {
    jQuery('input.postal-code', context).on('autocompleteclose', this.fillLocality);
  }
};
